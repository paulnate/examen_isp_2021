/*
Subject 2:
* Implement a Java application running on 2 threads.
*  The thread names will be StrThread1 or StrThread2.
* The threads will display 5 messages in the console, at intervals of 5 sec.
*  The message will be of the form [thread_name] - [current_time], where [thread_name] will be StrThread1 or StrThread2.
* */

import java.time.LocalDateTime;

    class StrThread extends Thread {
        private String name;

        StrThread(String name) {
            super(name);
        }

        public void run() {
            for (int i = 0; i < 5; i++) {
                System.out.println(getName() + " - " + LocalDateTime.now());
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
        }

    }
    class TestClass{
        public static void main(String[] args) {

            StrThread StrThread1 = new StrThread("StrThread1");
            StrThread StrThread2 = new StrThread("StrThread2");

            StrThread1.start();
            StrThread2.start();
        }
    }

